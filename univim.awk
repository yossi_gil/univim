#!/usr/bin/gawk -f 
BEGIN {
  verbose=1
  OFS=","
  FS=","
  RS="(\r\n)|\n"
  mode=="" && mode = "p2u"
}

function isConserve(s) {
  gsub(/\\[0-9]+/, "", s) 
  return s in conserve
}

function p2u(pattern, replacement) {
  if (mode != "p2u") 
    return
  print pattern, replacement, class[latex], latex, unicode, unicode2number[unicode], stamp[latex] | "sort"
}

function normalize(classification, normalized) {
  if (normalized == "")
    normalized = latex
  class[unicode2latex[unicode] = latex = normalized] = classification
  if (!(latex in stamp))
    stamp[latex] = FILENAME "(" FNR ")" 
  print (latex "->" unicode "\t" c "\t"  classification "\t" uncidoe2number[unicode] "\t" stamp[latex]) | "column -t > uniuse.log" 
}

function u2l(unicode, latex) {
  if (mode != "u2l") 
    return
  print class[latex], maxUnicode, unicode, maxLatex,latex, stamp[latex], unicode2number[unicode] | "sort"
}

function allHeadsAllTails(pattern, replacement, h, t) {
  for (h in head) 
    for (t in tail)
      p2u(h pattern t, "\\1" replacement "\\3")
}

BEGIN {
  ALL=1
  ebslash = "\\\\"
  notBackslash = "[^\\\\]"
  notSpecial = "[^\\\\^]"
  eopen = "\\("
  eclose = "\\)"
  emptyGroup = eopen eclose
  head["^" emptyGroup emptyGroup] 
  head[eopen notBackslash eopen ebslash ebslash eclose "*" eclose]   
  tail["\\(\\)\\(\\)$"]
  tail["\\([^a-zA-Z]\\(\\)\\)"]
  ghead["^" emptyGroup emptyGroup] 
  ghead[eopen notSpecial eopen ebslash ebslash eclose "*" eclose]   
}

NR == 1 {
  for(i = 1; i <= NF; ++i) {
    name2number[$i] = i 
    number2name[i] = $i 
  }
  next
}

function field(s) {
  s in name2number || die("Invalid field name: " s)
  return trim($(name2number[s]))
}

function trim(s) {
  gsub("^ +","",s)
  gsub(" +$","",s)
  return s
}

# Retrieve 'number', 'latex' and 'unicode' components
ALL {
  number = field("Unicode Decimal")
  latex = field("Latex")
  unicode = field("Symbol")
  if (number > 65535) 
    unicode = sprintf("%c",number)
  if (field("Conserve") == "T") 
    conserve[unicode]
  normalize("raw Conserve="(unicode in conserve))
  (unicode in unicode2latex) || (unicode2latex[unicode] = latex)
  (unicode in unicode2number) || (unicode2number[unicode] = number)
  maxUnicode >= length(unicode) || (maxUnicode = length(unicode)) 
}

# e.g., \sqrt[3]
latex~/^\\[a-zA-Z]+ *\[ *[^\[\] ]* *\]$/ { 
  command = gensub(/ *\[ *[^\[\] ]* *\]$/, "", 1, latex)
  argument = gensub(/^\\[a-zA-Z]+ *\[ */, "", 1, latex)
  argument = gensub(/ *\]$/, "", 1, argument)
  argument = cleanup(argument)
  command = cleanup(command)
  normalize("\\macro[characters]", command"["argument"]")
  option[latex]=unicode
  for (h in head)
    p2u(h escape(latex),  "\\1" unicode)
  next
}


# e.g., \alpha, or even \beta{}, which is also legitimate.
# (we are second, to give \sqrt[2] its chance first)
latex~/^\\[a-zA-Z]+[ {}]*$/ { 
  normalize("\\macro",  gensub("[ {}]", "", "g", latex)) 
  plain[latex] = unicode
  allHeadsAllTails(escape(latex), unicode)
  next
}

# e.g., ^2, ^A, but not ^\beta ^\alpha
# or, e.g., ^{2}, ^{A}, but not ^{\beta} ^{\alpha}
latex~/^\^ *[^\\ ]$/ || latex~/^\^ *{ *[^\\ {}] *}$/ {
  normalize("^character", "^" cleanup(body(latex)))
  s = body(latex)
  for (h in ghead) {
    p2u(h "\\^ *" s , "\\1" unicode)
    p2u(h "\\^ *{ *" s " *}", "\\1" unicode)
  }
  next
}

# e.g., _2, _A, but not _\beta 
# or, e.g., _{2}, _{A}, but not _{\beta} _{\alpha}
latex~/^_ *[^\\ ]$/ || latex~/^_ *{ *[^\\ {}] *}$/ {
  normalize("_character", "_" cleanup(body(latex)))
  s = body(latex)
  for (h in head) {
    p2u(h "_ *" s , "\\1" unicode)
    p2u(h "_ *{ *" s " *}", "\\1" unicode)
  }
  next
}

function cleanup(s) {
  return gensub(/[ {}]/, "", "g", s) 
}

# e.g., \mathbb{Z}, \mathbb C
latex~/^\\[a-zA-Z]+ +[a-zA-Z]$/ || latex~/^\\[a-zA-Z]+ *{ *[a-zA-Z] *}$/ {
  argument = gensub(/^\\[a-zA-Z]+ */, "", 1, latex) # Remove prefix
  command = substr(latex, 1, length(latex) - length(argument)) 
  argument = cleanup(argument) 
  command = cleanup(command) 
  normalize("\\macro{letter}", command "{" argument "}")
  for (h in head) {
    p2u(h escape(command) " +" argument, "\\1" unicode)
    p2u(h escape(command) " *{" argument " *}", "\\1" unicode)
  }
  next
}


# e.g., \not{<}  \not<, but not \not\equiv
latex~/^\\[a-zA-Z]+ *{[^{}a-zA-Z]}$/ ||  latex~/^\\[a-zA-Z]+ *[^{}a-zA-Z]$/ {
  argument = gensub(/^\\[a-zA-Z]+ */, "", 1, latex) # Remove prefix
  command = substr(latex, 1, length(latex) - length(argument)) 
  argument = cleanup(argument) 
  command = cleanup(command) 
  normalize("\\macro+punctuation", command argument)
  for (h in head) {
    p2u(h escape(command) " *" argument, "\\1" unicode)
    p2u(h escape(command "{" argument "}"), "\\1" unicode )
  }
  next
}

# e.g., \not{\equiv} or \not\le but not \not{<} nor \not>
# e.g., or, e.g., \not\equiv
latex~/^\\[a-zA-Z]+ *{ *\\[a-zA-Z]+ *}$/ || latex~/^\\[a-zA-Z]+ *\\[a-zA-Z]+$/ {
  argument = gensub(/^\\[a-zA-Z]+ */, "", 1, latex)
  command = substr(latex, 1, length(latex) - length(argument)) 
  argument = cleanup(argument) 
  command = cleanup(command) 
  normalize("\\macro\\macro", command argument) 
  macro[latex] = unicode 
  allHeadsAllTails(escape(latex), unicode)
  c = escape(command) 
  a = escape(argument) 
  for (h in head) 
    for (t in tail) 
      p2u(h c " *" a t, "\\1" unicode "\\3")
  for (h in head) 
    p2u(h c " *{ *" a " *}", "\\1" unicode)
  next
}

# e.g., \$, \%, \$
latex~/^\\[^a-zA-Z]$/ { 
  normalize("\\character")
  for (h in head) 
    p2u(h escape(latex), "\\1" unicode)
  next
}


# e.g., ^\beta, ^{\phi}, but not ^i, ^2, ^A
latex~/^\^[{} ]*\\[a-zA-Z]+[{} ]*$/ {
  normalize("^\\macro", "^" cleanup(body(latex)))
  superscript[latex] = s = body(latex)
  for (h in ghead) 
    for (t in tail) 
      p2u(h "\\^ *" s t, "\\1" unicode "\\3")
  for (h in ghead) 
    p2u(h  "\\^ *{ *" s " *}", "\\1" unicode)
  next
}


# e.g., _\beta, _phi, but not _i, _2, _A
latex~/^_[{} ]*\\[a-zA-Z]+[{} ]*$/ {
  normalize("_\\macro", "_" cleanup(body(latex)))
  subscript[latex] = s = body(latex)
  for (h in head) 
    for (t in tail) 
      p2u(h "_ *" s t, "\\1" unicode "\\3")
  for (h in head) 
    p2u(h  "_ *{ *" s " *}", "\\1" unicode)
  next
}

# e.g., ", ``, ' 
latex~/^[[:punct:][:digit:]]+$/ {
  normalize("sequence")
  p2u(latex, unicode)
  next
}

# all the rest
{
  normalize("unknown")
  next
}

function openingMacro(s, rest) {
  __("input to opening Macro: " s)
  rest = s
  sub(/^\\[a-zA-Z]+ */,"", rest) 
  __("s/rest is: " s "/" rest)
  s = body(latex) 
  __("openingMacro/rest is: " s "/" rest)
  sub(/ +$/, "",s)
  __("output latex opening Macro: " s)
  return s
}

END {
  for (unicode in unicode2latex) {
    latex = unicode2latex[unicode] 
    maxLatex >= length(latex) || (maxLatex = length(latex)) 
  }
}

END {
  print_shortcuts()
  print_undos()
} 


function print_undos() {
  for (unicode in unicode2latex) 
    u2l(unicode, unicode2latex[unicode]) 
}

function makeLineVim(fmt1,fmt2, latex, unicode) {
  fmt1 = sprintf(fmt1, unicode, esc(latex)) 
  return sprintf(fmt2,  maxUnicode + maxLatex + 4, fmt1, stamp[latex]) 
}

function print_shortcuts() {
  for (latex in option) {
    m = openingMacro(latex) 
    if (!(m in plain) || plain[m] == "") 
      continue
    m = plain[m]
    unicode = option[latex]
    latex = gensub(/^\\[a-zA-Z]+ */, "", 1, latex) 
    _("unicode[option]") 
    p2u(escape(latex), unicode)
  }
  for (commandArgument in macro) {
    unicode = macro[commandArgument]
    command = openingMacro(commandArgument)
    argument = trim(substr(commandArgument, length(command) + 1))
    if (argument in plain && plain[argument] != "") {
      argument = plain[argument]
      normalize("\\macro{unicode}", command "{" argument "}")
      c = escape(command)
      allHeadsAllTails(c  " *" argument, unicode)
      allHeadsAllTails(c " *{ *" argument " *}", unicode)
    }
  }
  for (s in superscript) {
    if (! (s in plain) || plain[s] == "") 
      continue
    unicode = superscript[s]
    s = plain[s]
    normalize("^unicode") 
    p2u(escape(latex), unicode)
    for (h in ghead) {
      p2u(h escape("^") " *" s, "\\1" unicode)
      p2u(h escape("^") " *{ *" s " *}", "\\1" unicode)
    }
  }
  for (s in subscript) {
    if (! (s in plain) || plain[s] == "") 
      continue
    unicode = subscript[s]
    s = plain[s]
    normalize("_unicode") 
    for (h in head) {
      p2u(h "_ *" s, "\\1" unicode)
      p2u(h "_ *{ *" s " *}", "\\1" unicode)
    }
  }
}

function esc(s, i, c, result) {
  result = ""
  for (i = 1; i <= length(s); ++i) {
    c = substr(s, i, 1)
    if (c == "\\") { 
      result = result "\\\\" 
      continue
    }
    result = result c
  }
  return result
}

function escape_character(c) {
  if (c == "*")  
    return "\\*"
  if (c == "\\")  
    return "\\\\"
  return c
}

function escape(s, i, c, result) {
  __("input to escape: " s)
  result = ""
  for (i = 1; i <= length(s); ++i) {
    c = substr(s, i, 1)
    if (c == "\\") { 
      result = result "\\\\" 
      continue
    }
    if (c ~ /[*|]/) { 
      result = result "\\" c ""
      continue
    }
    if (c == "[" ) { 
      result = result " *\\[ *"
      continue
    }
    if (c == "]" ) { 
      result = result " *\\]"
      continue
    }
    if (c == "{" ) { 
      result = result " *{ *"
      continue
    }
    if (c == "}" ) { 
      result = result " *}"
      continue
    }
    result = result c
  }
  return result
}

function __(s1, s2) {
  print FILENAME "(" FNR ")", s1, s2 > "/dev/null"
}
function body(s) {
  return substr(s,2)
}
