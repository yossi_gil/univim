PROGRAM = univim.awk
DEFINITIONS = univim.csv
U = univim
L = latex
P = pattern
TARGETS := $P2$U.vim $U2$L.vim $U.sty tex.vim
TARGETS += examples 
AUXILIARY = classes.tsv u2l.csv u2l.csv p2u.csv 


.PHONY: all 
all: $(TARGETS) 

$(PROGRAM): Makefile
	touch $@

u2l.csv: $(PROGRAM) $(DEFINITIONS)
	chmod +x $<
	$< --assign 'mode=$(basename $@)' $(DEFINITIONS) > $@ 	 

p2u.csv: $(PROGRAM) $(DEFINITIONS) 
	chmod +x $<
	$< --assign 'mode=$(basename $@)' $(DEFINITIONS) > $@ 	 

classes.tsv: p2u.csv
	awk -F , '{print $$3}' $< | sort | uniq -c | sort -n > $@

%: %.u2l.sh u2l.csv
	chmod +x $< 
	$^ > $@

%: %.p2u.sh p2u.csv
	chmod +x $<
	$^ > $@

###############################################################################
# Clean 
###############################################################################
# vim: +2,/^\s*)\s*)\s*$/-1!column -t | sort -u | awk '{print "\t" $0}'
garbage := $(sort $(wildcard \
	*~                       \
	*.aux                    \
	$(AUXILIARY)             \
	*.backup                 \
	*.bak                    \
	*.bbl                    \
	*.bcf                    \
	*.blg                    \
	*blx.bib                 \
	*.class                  \
	*[dD][eE][lL][mM][eE]*   \
	*.detexed                \
	*.dvi                    \
	*.fdb_                   \
	*.fdb_latexmk            \
	*.fls                    \
	*gnuplottex-fig[0-9]*.*  \
	*.listing                \
	*.log                    \
	$(MAIN).pdf              \
	*.o                      \
	*.out                    \
	*.run.xml                \
	.*swo                    \
	.*swp                    \
	*.synctex.gz             \
	$(TARGET)                \
	$(TARGETS)               \
	*.vtc                    \
))
NG := $(words $(garbage)) 
BACKUP := $(shell mktemp -d)

.PHONY: clean 
clean: $(call tip,clean,\
	move all auotmatically-generated-files (currently $(NG)) to /tmp)
	@ [ -z '$(garbage)' ] || mv -f $(garbage) $(BACKUP)
	@ [ $(NG) -gt 1 ] && echo $(NG) files moved to $(BACKUP)  || true 
	@ [ $(NG) -eq 1 ] && echo $(NG) file moved to $(BACKUP)  || true 
	@ [ $(NG) -eq 0 ] && echo already clean || true 

.PHONY: rebuild 
rebuild: clean all

.PHONY: fromScratch 
fromScratch: clean 
	fromScratch: $(call tip,clean,\
		type  all auotmatically-generated-files (currently $(NG)) to /tmp)

.PHONY: examples
examples: multiple-accumulating-sub-super-scripts.pdf

multiple-accumulating-sub-super-scripts.pdf: multiple-accumulating-sub-super-scripts.tex
	xelatex $^
